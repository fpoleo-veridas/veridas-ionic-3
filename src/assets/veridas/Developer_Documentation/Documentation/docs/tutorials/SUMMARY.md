## Description

VDDocument captures document images, guiding the user in a self-contained and accessible system.
VDDocument retrieves the following information:

- **Document obverse** (front side) image of all supported documents.
- **Document reverse** (back side) image for supported two-sided documents.

VDDocument takes into account the type of device being used and attempts to launch the camera (front facing or rear) that will provide the user with the best experience:

- **Rear camera** is used on mobile devices (smartphones, tablets…)
- **Front camera** is used on computers (desktops, laptops…)

The following permission is required for the framework to work:

- Camera.

To improve user experience, VDDocument attempts to capture images of the document automatically, by having the user center each side in a frame. VDDocument uses several algorithms to detect a document in the camera’s field of vision, and snaps the photo without need for further user action. Also, the user can take a photo manually by touching/clicking on the framed document.

The camera attempts to start at Full HD (1080p), but lower resolutions are intended whether the devices are unable to support the optimal one.

The photo is taken in .JPG format, being a 1080p capture about 500KB on size.

## Specifications

The SDK supports the following devices and browsers:

| Browser Name        | Minimum version | Current version |
| :------------------ | :-------------: | :-------------: |
| Chrome              |       57        |       90        |
| Firefox             |       52        |       85        |
| Safari              |      11.2       |       TP        |
| Opera               |       44        |       72        |
| Edge                |       16        |       87        |
| Chrome for Android  |       57        |       87        |
| Firefox for Android |       52        |       83        |
| iOS Safari          |      11.2       |      14.3       |
| Opera Mobile        |       46        |       59        |
| Samsung Internet    |       7.2       |       13        |

Current version at 2021/01/25 [GetUserMedia](https://caniuse.com/#search=getUserMedia) [WebAssembly](https://caniuse.com/#search=webassembly)

The size of the SDK is **3 MB**

Additionally, some dependencies will be needed. These are:

- cv.js: 4.83 MB
- docLibHelper.js: 1.1 MB

## Integration

VDDocument is built as a stand-alone module with a single external dependency:

- **VDDocument.js** file contains the core functionality.
- **cv.js** file contains the external dependency.
- **docLibHelper** folder with read bidi codes library (docLibHelper.js).

Place VDDocument.js and cv.js with the other static assets of your website.
Place docLibHelper folder in main root of the project. It is not necessary to add this library in a script tag. In case file inside this folder need to be stored in different paths, it needs to specify the path under the required configuration object `docLibHelperPath`.

Example of structure:

- public
  - docLibHelper/docLibHelper.js
  - opencv/cv.js
  - index.html
  - VDDocument.js

In your HTML, for a correct operation with Safari on iPhone it is necessary that the HTML begins with the `<!DOCTYPE html>` tag. Moreover, right before the closing tag of the body element, place two script tags:

```javascript
<script type="application/javascript" src="path_to_static_assets/VDDocument.js" charset="UTF-8"></script>
<script type="application/javascript" src="path_to_static_assets/cv.js" onerror="CVLoadError()"></script>
```

At the location in your HTML where VDDocument should mount, place a div element with a unique id or class name. The id “target” is used here as an example:

```javascript
<div id='target'></div>
```

The target must have defined width and height sizes values; here is an example taking relative values from the parent node:

```javascript
#target {
  width: 100%,
  height: 100%,
}
```

The following code shows how to mount the SDK with the minimal required configuration:

```javascript
function sdkStart() {
  const VDDocument = makeVDDocumentWidget();
  VDDocument({
    targetSelector: '#target',
    documents: ['ES2'],
  });
}
```

Before sdkStart function, it is needed a pre-load of the models that uses cv.wasm for detecting documents. The next picture shows this calling process:

```javascript
try {
  var Module = {
    onRuntimeInitialized: function () {
      if (window.cv instanceof Promise) {
        window.cv.then(target => {
          window.cv = target;
          sdkStart();
        });
      } else {
        // for backward compatible
        sdkStart();
      }
    },
  };
} catch (e) {
  console.error(e);
}
```

Example

```javascript
<body>
  <!-- installation of sdk starts -->
  <div id="target"></div>
  <script>
    function sdkStart() {
      const VDDocument = makeVDDocumentWidget();
      VDDocument({
        targetSelector: '#target',
        documents: [
          'ES2',
        ],
        docLibHelperPath: '/docLibHelper'
      });
    }
    function CVLoadError() {
      Module.printErr('Failed to load/initialize cv.js');
    }
    try {
      var Module = {
        onRuntimeInitialized: function () {
          if (window.cv instanceof Promise) {
            window.cv.then((target) => {
              window.cv = target;
              sdkStart();
            })
          } else {
            sdkStart();
          }
        }
      }
    } catch (e) {
      console.error(e)
    }
  </script>
  <script type="application/javascript" src="../assets/sdk/VDDocument.js" charset="UTF-8"></script>
  <script type="application/javascript" src="../assets/sdk/cv.js" onerror="CVLoadError()"></script>
  <!-- installation of sdk ends -->
</body>

```

## Integration in Angular

In file, angular.json:  
 "assets": [
"src/favicon.ico",
"src/assets",
{
"glob": "docLibHelper.js",
"input": "src/assets/docLibHelper/",
"output": "/docLibHelper/"
},
],  
 "scripts": [
"src/assets/libs/VDDocument.js",
"src/assets/libs/cv_init.js",
"src/assets/libs/cv.js",
"src/assets/docLibHelper/docLibHelper.js"
],

In folder, src/assets/libs add new file cv_init.js, with this code:

```javascript
try {
  var Module = {
    onRuntimeInitialized: function () {
      if (window.cv instanceof Promise) {
        window.cv.then(target => {
          window.cv = target;
          sdkStart();
        });
      } else {
        sdkStart();
      }
    },
  };
} catch (e) {
  console.error(e);
}
```

## Use

The SDK gives one global method called **getSDKversion**, this method when called, it will return the version of the SDK.

Once VDDocument mounts, it can be unmounted programmatically by invoking the global function **destroyVDDocumentWidget**.

VDDocument unmounts itself after completing a successful detection of the document, or after detection times out. You can control how long it takes for the process to time out to a certain extent. Refer to the configuration section.

Other events are available from SDK that are listed on Type Definitions section.
